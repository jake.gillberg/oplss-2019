%default total

data Proposition : Type where
  Atom : Type -> Proposition
  Plus : Proposition -> Proposition -> Proposition
  With : Proposition -> Proposition -> Proposition

data Sequent : Proposition -> Proposition -> Type where
  ID     : Sequent a a
  PlusR1 : Sequent a b ->                Sequent a (Plus b c)
  PlusR2 : Sequent a c ->                Sequent a (Plus b c)
  PlusL  : Sequent a c -> Sequent b c -> Sequent (Plus a b) c
  WithL1 : Sequent a c ->                Sequent (With a b) c
  WithL2 : Sequent b c ->                Sequent (With a b) c
  WithR  : Sequent a b -> Sequent a c -> Sequent a (With b c)
  Cut    : Sequent a b -> Sequent b c -> Sequent a c

namespace cutFree

  data CutFreeSequent : Proposition -> Proposition -> Type where
    ID     : CutFreeSequent a a
    PlusR1 : CutFreeSequent a b -> CutFreeSequent a (Plus b c)
    PlusR2 : CutFreeSequent a c -> CutFreeSequent a (Plus b c)
    PlusL  : CutFreeSequent a c ->
             CutFreeSequent b c -> CutFreeSequent (Plus a b) c
    WithL1 : CutFreeSequent a c -> CutFreeSequent (With a b) c
    WithL2 : CutFreeSequent b c -> CutFreeSequent (With a b) c
    WithR  : CutFreeSequent a b ->
             CutFreeSequent a c -> CutFreeSequent a (With b c)

cut_admissible : CutFreeSequent a b -> CutFreeSequent b c -> CutFreeSequent a c
cut_admissible ID y = y
cut_admissible x ID = x
cut_admissible (PlusR1 d) (PlusL e f) = cut_admissible d e
cut_admissible (PlusR2 d) (PlusL e f) = cut_admissible d f
cut_admissible (PlusL d e) f = PlusL (cut_admissible d f) (cut_admissible e f)
cut_admissible (WithR d e) (WithL1 f) = cut_admissible d f
cut_admissible (WithR d e) (WithL2 f) = cut_admissible e f
cut_admissible (WithL1 d) e = WithL1 (cut_admissible d e)
cut_admissible (WithL2 d) e = WithL2 (cut_admissible d e)
cut_admissible x (PlusR1 y) = PlusR1 (cut_admissible x y)
cut_admissible x (PlusR2 y) = PlusR2 (cut_admissible x y)
cut_admissible x (WithR y z) = WithR (cut_admissible x y) (cut_admissible x z)

cut_elim : Sequent a b -> CutFreeSequent a b
cut_elim ID = ID
cut_elim (PlusR1 x) = PlusR1 (cut_elim x)
cut_elim (PlusR2 x) = PlusR2 (cut_elim x)
cut_elim (PlusL x y) = PlusL (cut_elim x) (cut_elim y)
cut_elim (WithL1 x) = WithL1 (cut_elim x)
cut_elim (WithL2 x) = WithL2 (cut_elim x)
cut_elim (WithR x y) = WithR (cut_elim x) (cut_elim y)
cut_elim (Cut x y) = cut_admissible (cut_elim x) (cut_elim y)

namespace atomicID
  data AtomicIDSequent : Proposition -> Proposition -> Type where
    ID     : (a : Type) -> AtomicIDSequent (Atom a) (Atom a)
    PlusR1 : AtomicIDSequent a b -> AtomicIDSequent a (Plus b c)
    PlusR2 : AtomicIDSequent a c -> AtomicIDSequent a (Plus b c)
    PlusL  : AtomicIDSequent a c ->
             AtomicIDSequent b c -> AtomicIDSequent (Plus a b) c
    WithL1 : AtomicIDSequent a c -> AtomicIDSequent (With a b) c
    WithL2 : AtomicIDSequent b c -> AtomicIDSequent (With a b) c
    WithR  : AtomicIDSequent a b ->
             AtomicIDSequent a c -> AtomicIDSequent a (With b c)
    Cut    : AtomicIDSequent a b ->
             AtomicIDSequent b c -> AtomicIDSequent a c

identity_expansion : (a : Proposition) -> AtomicIDSequent a a
identity_expansion (Atom x) = ID x
identity_expansion (Plus x y) = PlusL (PlusR1 (identity_expansion x))
                                      (PlusR2 (identity_expansion y))
identity_expansion (With x y) = WithR (WithL1 (identity_expansion x))
                                      (WithL2 (identity_expansion y))

complete : Sequent a b -> AtomicIDSequent a b
complete ID {a} = identity_expansion a
complete (PlusR1 x) = PlusR1 (complete x)
complete (PlusR2 x) = PlusR2 (complete x)
complete (PlusL x y) = PlusL (complete x) (complete y)
complete (WithL1 x) = WithL1 (complete x)
complete (WithL2 x) = WithL2 (complete x)
complete (WithR x y) = WithR (complete x) (complete y)
complete (Cut x y) = Cut (complete x) (complete y)

partial
Bits : Proposition
Bits = Plus Bits Bits

flip : Sequent Bits Bits
flip = PlusL (PlusR2 {b = Bits} flip) (PlusR1 {c = Bits} flip)
