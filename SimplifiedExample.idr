%default total

data Proposition : Type where
  Atom : Type -> Proposition
  Plus : Proposition -> Proposition -> Proposition
  With : Proposition -> Proposition -> Proposition

data Sequent : Proposition -> Proposition -> Type where
  ID     : Sequent a a
  PlusR1 : Sequent a b ->                Sequent a (Plus b c)
  PlusR2 : Sequent a c ->                Sequent a (Plus b c)
  PlusL  : Sequent a c -> Sequent b c -> Sequent (Plus a b) c
  WithL1 : Sequent a c ->                Sequent (With a b) c
  WithL2 : Sequent b c ->                Sequent (With a b) c
  WithR  : Sequent a b -> Sequent a c -> Sequent a (With b c)
  Cut    : Sequent a b -> Sequent b c -> Sequent a c

partial
Bits : Proposition
Bits = Plus Bits Bits

flip : Sequent Bits Bits
flip = PlusL (PlusR2 {b = Bits} flip) (PlusR1 {c = Bits} flip)
